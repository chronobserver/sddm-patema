# Patema Inverted Theme for SDDM

[SDDM][sddm] is a Login Manager for Linux which can be themed via qml.<br>
This theme is based on [sddm-deepin][] but uses a Patema Inverted background.

[sddm]: https://github.com/sddm/sddm
[sddm-deepin]: https://github.com/Match-Yang/sddm-deepin

## Screenshot

![screenshot](screenshot.png)

## Installation

```sh
git clone https://git.disroot.org/ObserverOfTime/sddm-patema
cd sddm-patema
sddm-greeter --test-mode --theme . # preview
sudo cp -r . /usr/share/sddm/themes/patema # install
```

## License

[CC BY-SA 3.0](COPYING)
